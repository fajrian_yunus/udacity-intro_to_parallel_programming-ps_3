/* Udacity Homework 3
   HDR Tone-mapping

  Background HDR
  ==============

  A High Dynamic Range (HDR) image contains a wider variation of intensity
  and color than is allowed by the RGB format with 1 byte per channel that we
  have used in the previous assignment.  

  To store this extra information we use single precision floating point for
  each channel.  This allows for an extremely wide range of intensity values.

  In the image for this assignment, the inside of church with light coming in
  through stained glass windows, the raw input floating point values for the
  channels range from 0 to 275.  But the mean is .41 and 98% of the values are
  less than 3!  This means that certain areas (the windows) are extremely bright
  compared to everywhere else.  If we linearly map this [0-275] range into the
  [0-255] range that we have been using then most values will be mapped to zero!
  The only thing we will be able to see are the very brightest areas - the
  windows - everything else will appear pitch black.

  The problem is that although we have cameras capable of recording the wide
  range of intensity that exists in the real world our monitors are not capable
  of displaying them.  Our eyes are also quite capable of observing a much wider
  range of intensities than our image formats / monitors are capable of
  displaying.

  Tone-mapping is a process that transforms the intensities in the image so that
  the brightest values aren't nearly so far away from the mean.  That way when
  we transform the values into [0-255] we can actually see the entire image.
  There are many ways to perform this process and it is as much an art as a
  science - there is no single "right" answer.  In this homework we will
  implement one possible technique.

  Background Chrominance-Luminance
  ================================

  The RGB space that we have been using to represent images can be thought of as
  one possible set of axes spanning a three dimensional space of color.  We
  sometimes choose other axes to represent this space because they make certain
  operations more convenient.

  Another possible way of representing a color image is to separate the color
  information (chromaticity) from the brightness information.  There are
  multiple different methods for doing this - a common one during the analog
  television days was known as Chrominance-Luminance or YUV.

  We choose to represent the image in this way so that we can remap only the
  intensity channel and then recombine the new intensity values with the color
  information to form the final image.

  Old TV signals used to be transmitted in this way so that black & white
  televisions could display the luminance channel while color televisions would
  display all three of the channels.
  

  Tone-mapping
  ============

  In this assignment we are going to transform the luminance channel (actually
  the log of the luminance, but this is unimportant for the parts of the
  algorithm that you will be implementing) by compressing its range to [0, 1].
  To do this we need the cumulative distribution of the luminance values.

  Example
  -------

  input : [2 4 3 3 1 7 4 5 7 0 9 4 3 2]
  min / max / range: 0 / 9 / 9

  histo with 3 bins: [4 7 3]

  cdf : [4 11 14]


  Your task is to calculate this cumulative distribution by following these
  steps.

*/


#include "reference_calc.cpp"
#include "utils.h"
#include "stdio.h"

__global__ void get_minimum(const float* const d_logLuminance, 
                            float* d_min_logLum, 
                            const unsigned int d_logLuminance_length) {
    //the correct value is -3.109206
    
    extern __shared__ float sdata[];
    
    unsigned int myId = threadIdx.x + blockDim.x * blockIdx.x;
    unsigned int tid = threadIdx.x;
    
    sdata[tid] = d_logLuminance[myId];
    __syncthreads();
    
    for (unsigned int s = blockDim.x / 2 ; s > 0 ; s >>= 1) {
        if (tid < s) {
            sdata[tid] = min(sdata[tid+s], sdata[tid]);
        }
        __syncthreads();
    }
    
    if (tid == 0) {
        d_min_logLum[blockIdx.x] = sdata[0];
    }
}

float get_minimum_naive(const float* arr, const unsigned int length) {
    float output = 0;
    for (unsigned int i = 0 ; i < length ; i++) {
        if (i == 0) {
            output = arr[i];
        } else {
            output = min(output, arr[i]);
        }
    }
    return output;
}

__global__ void get_maximum(const float* const d_logLuminance, 
                            float* d_max_logLum, 
                            const unsigned int d_logLuminance_length) {
    
    extern __shared__ float sdata[];
    
    unsigned int myId = threadIdx.x + blockDim.x * blockIdx.x;
    unsigned int tid = threadIdx.x;
    
    sdata[tid] = d_logLuminance[myId];
    __syncthreads();
    
    for (unsigned int s = blockDim.x / 2 ; s > 0 ; s >>= 1) {
        if (tid < s) {
            sdata[tid] = max(sdata[tid+s], sdata[tid]);
        }
        __syncthreads();
    }
    
    if (tid == 0) {
        d_max_logLum[blockIdx.x] = sdata[0];
    }
}

float get_maximum_naive(const float* arr, const unsigned int length) {
    float output = 0;
    for (unsigned int i = 0 ; i < length ; i++) {
        if (i == 0) {
            output = arr[i];
        } else {
            output = max(output, arr[i]);
        }
    }
    return output;
}

__global__ void doSemiBinningNew(const float* const d_logLuminance,  
                    unsigned int* const d_semi_hist,
                    const float minToMaxRange, 
                    const float minimumValue, 
                    const int numBins,
                    const unsigned int logLuminance_length,
                    const unsigned int semi_bins_length) {
    unsigned int myId = threadIdx.x + blockDim.x * blockIdx.x;
    unsigned int my_bin = (int) (((float) numBins) * (d_logLuminance[myId] - minimumValue) / minToMaxRange);
    unsigned int semiBinIndex = myId % semi_bins_length;
    atomicAdd(&(d_semi_hist[semiBinIndex * numBins + my_bin]), 1);
}

__global__ void sum_semi_hist(unsigned int* const d_semi_hist,  
                    unsigned int* const d_hist,
                    const float minToMaxRange, 
                    const float minimumValue, 
                    const int numBins,
                    const unsigned int logLuminance_length,
                    const int numberOfThreads) {
    
    extern __shared__ unsigned int sdata_sum_semi_hist[];
    unsigned int tid = threadIdx.x;
    sdata_sum_semi_hist[tid] = d_semi_hist[numBins * tid + blockIdx.x];
    __syncthreads();
    
    for (unsigned int s = blockDim.x/2 ; s > 0 ; s >>= 1) {
        if (tid < s) { 
            sdata_sum_semi_hist[tid] += sdata_sum_semi_hist[tid+s];
        }
        __syncthreads();
    }
    
    if (tid == 0) {
        d_hist[blockIdx.x] = sdata_sum_semi_hist[tid];
    }
}

__global__ void calculate_cdf(unsigned int* const d_hist, unsigned int* const d_cdf, const unsigned int numBins) {
    extern __shared__ unsigned int sdata_cdf[];
    
    const unsigned int tid = threadIdx.x;
    const unsigned int maxDistance = numBins >> 1;
    
    sdata_cdf[tid] = d_hist[tid];
    __syncthreads();
    
    for (unsigned int distance = 1 ; distance <= maxDistance ; distance <<= 1) {
        unsigned int new_value = 0;
        if (tid < distance) {
            new_value = sdata_cdf[tid];
        } else {
            new_value = sdata_cdf[tid-distance] + sdata_cdf[tid];
        }
        __syncthreads();
        sdata_cdf[tid] = new_value;
        __syncthreads();
    }
    
    d_cdf[tid] = sdata_cdf[tid];
}

void your_histogram_and_prefixsum(const float* const d_logLuminance,
                                  unsigned int* const d_cdf,
                                  float &min_logLum,
                                  float &max_logLum,
                                  const size_t numRows,
                                  const size_t numCols,
                                  const size_t numBins)
{
    const int threadPerBlockForMaxMin = 1024;
    const dim3 blockSize(threadPerBlockForMaxMin);
    const unsigned int d_logLuminance_length = numRows * numCols;
    unsigned int gridSizeForMaxMin = d_logLuminance_length / threadPerBlockForMaxMin;
    if (threadPerBlockForMaxMin * gridSizeForMaxMin < d_logLuminance_length) {
        gridSizeForMaxMin++;
    }
    const dim3 gridSize(gridSizeForMaxMin);
    
    float *d_min_logLum;
    checkCudaErrors(cudaMalloc(&d_min_logLum, sizeof(float) * gridSizeForMaxMin));
    get_minimum<<<gridSizeForMaxMin, blockSize, threadPerBlockForMaxMin*sizeof(float)>>>(d_logLuminance, d_min_logLum, d_logLuminance_length);
    float *h_min_loglum = (float *) malloc(sizeof(float) * gridSizeForMaxMin);
    checkCudaErrors(cudaMemcpy((void *)  h_min_loglum, (void *)  d_min_logLum, sizeof(float) * gridSizeForMaxMin, cudaMemcpyDeviceToHost));
    min_logLum = get_minimum_naive(h_min_loglum, gridSizeForMaxMin);
    free(h_min_loglum);
    checkCudaErrors(cudaFree(d_min_logLum));
    
    float *d_max_logLum;
    checkCudaErrors(cudaMalloc(&d_max_logLum, sizeof(float) * gridSizeForMaxMin));
    get_maximum<<<gridSizeForMaxMin, blockSize, threadPerBlockForMaxMin*sizeof(float)>>>(d_logLuminance, d_max_logLum, d_logLuminance_length);   
    float *h_max_loglum = (float *) malloc(sizeof(float) * gridSizeForMaxMin);
    checkCudaErrors(cudaMemcpy((void *)  h_max_loglum, (void *)  d_max_logLum, sizeof(float) * gridSizeForMaxMin, cudaMemcpyDeviceToHost));
    max_logLum = get_maximum_naive(h_max_loglum, gridSizeForMaxMin);
    free(h_max_loglum);
    checkCudaErrors(cudaFree(d_max_logLum));
    
    const float minToMaxRange = max_logLum - min_logLum;
    
    
    const int numberOfSemiBins = 8;
    
    unsigned int* h_semi_hist = (unsigned int*) malloc(numberOfSemiBins * numBins * sizeof(unsigned int));
    for (unsigned int i = 0 ; i < numberOfSemiBins * numBins ; i++) {
        h_semi_hist[i] = 0;
    }
    unsigned int* d_semi_hist;
    checkCudaErrors(cudaMalloc(&d_semi_hist, sizeof(unsigned int) * numberOfSemiBins * numBins));
    checkCudaErrors(cudaMemcpy((void *)  d_semi_hist, (void *)  h_semi_hist, sizeof(unsigned int) * numberOfSemiBins * numBins, cudaMemcpyHostToDevice));
    doSemiBinningNew<<<gridSizeForMaxMin, blockSize>>>(d_logLuminance, d_semi_hist, minToMaxRange, min_logLum, numBins, d_logLuminance_length, numberOfSemiBins);
    
    
    unsigned int* h_hist = (unsigned int*) malloc(numBins * sizeof(unsigned int));
    for (unsigned int i = 0 ; i < numBins ; i++) {
        h_semi_hist[i] = 0;
    }
    unsigned int* d_hist;
    checkCudaErrors(cudaMalloc(&d_hist, sizeof(unsigned int) * numBins));
    checkCudaErrors(cudaMemcpy((void *)  d_hist, (void *)  h_hist, sizeof(unsigned int) * numBins, cudaMemcpyHostToDevice));
    sum_semi_hist<<<dim3(numBins), dim3(numberOfSemiBins), sizeof(unsigned int) * numBins * numberOfSemiBins>>>(d_semi_hist, d_hist, minToMaxRange, min_logLum, numBins, d_logLuminance_length, numberOfSemiBins);
    checkCudaErrors(cudaMemcpy((void *)  h_hist, (void *)  d_hist, sizeof(unsigned int) * numBins, cudaMemcpyDeviceToHost));
    calculate_cdf<<<dim3(1), dim3(numBins), sizeof(unsigned int) * numBins>>>(d_hist, d_cdf, numBins);
    
  //TODO
  /*Here are the steps you need to implement
    1) find the minimum and maximum value in the input logLuminance channel
       store in min_logLum and max_logLum
    2) subtract them to find the range
    3) generate a histogram of all the values in the logLuminance channel using
       the formula: bin = (lum[i] - lumMin) / lumRange * numBins
    4) Perform an exclusive scan (prefix sum) on the histogram to get
       the cumulative distribution of luminance values (this should go in the
       incoming d_cdf pointer which already has been allocated for you)       */
}
